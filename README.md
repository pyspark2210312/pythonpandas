+++++ ===== Python Pandas ===== +++++

This project is specifically created for python pandas practise.
1. Series vs Dataframe
	- Practised how to create a Dataframe from a numpy array
	- Practised how to fetch column and row wise data (loc and iloc for fetching row-wise data)
	- Analysed Series vs Dataframe difference
	- Practised how to put column data and Row data from Dataframe into list
2. Playing with the null values and other operations on Dataset
    - Practised with value_counts() methods on Dataframe, Dataframe Rows and Dataframe Columns
    - Practised with unique() methods on Dataframe, Dataframe Rows and Dataframe Columns
    - Practised with isnull() methods on Dataframe, Dataframe Rows and Dataframe Columns
    - Finding sum of null values
    - Filling null values
    - Iterating a dataframe using enumerate() function.